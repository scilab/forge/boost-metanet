#include <mex.h> 
#include <sci_gateway.h>
#include <api_scilab.h>
static int direct_gateway(char *fname,void F(void)) { F();return 0;};
extern Gatefunc sci_makeGraph;
extern Gatefunc sci_addNode;
extern Gatefunc sci_addEdge;
extern Gatefunc sci_printGraph;
extern Gatefunc sci_printVertex;
extern Gatefunc sci_getGraphName;
extern Gatefunc sci_getNodeNumber;
extern Gatefunc sci_getEdgeNumber;
extern Gatefunc sci_getNode;
extern Gatefunc sci_getHeadNodes;
extern Gatefunc sci_getTailNodes;
extern Gatefunc sci_getAllNodeLabels;
extern Gatefunc sci_getAllEdgeLabels;
extern Gatefunc sci_getAllEdgeWeights;
extern Gatefunc sci_setNodeLabel;
extern Gatefunc sci_setEdgeLabel;
extern Gatefunc sci_setEdgeWeight;
extern Gatefunc sci_getGraph;
extern Gatefunc sci_removeGraph;
extern Gatefunc sci_clearAllGraphs;
extern Gatefunc sci_breadthFirstSearch;
extern Gatefunc sci_depthFirstSearch;
extern Gatefunc sci_dijkstraSP;
extern Gatefunc sci_bellmanFordSP;
static GenericTable Tab[]={
  {(Myinterfun)sci_gateway,sci_makeGraph,"makeGraph"},
  {(Myinterfun)sci_gateway,sci_addNode,"addNode"},
  {(Myinterfun)sci_gateway,sci_addEdge,"addEdge"},
  {(Myinterfun)sci_gateway,sci_printGraph,"printGraph"},
  {(Myinterfun)sci_gateway,sci_printVertex,"printVertex"},
  {(Myinterfun)sci_gateway,sci_getGraphName,"getGraphName"},
  {(Myinterfun)sci_gateway,sci_getNodeNumber,"getNodeNumber"},
  {(Myinterfun)sci_gateway,sci_getEdgeNumber,"getEdgeNumber"},
  {(Myinterfun)sci_gateway,sci_getNode,"getNode"},
  {(Myinterfun)sci_gateway,sci_getHeadNodes,"getHeadNodes"},
  {(Myinterfun)sci_gateway,sci_getTailNodes,"getTailNodes"},
  {(Myinterfun)sci_gateway,sci_getAllNodeLabels,"getAllNodeLabels"},
  {(Myinterfun)sci_gateway,sci_getAllEdgeLabels,"getAllEdgeLabels"},
  {(Myinterfun)sci_gateway,sci_getAllEdgeWeights,"getAllEdgeWeights"},
  {(Myinterfun)sci_gateway,sci_setNodeLabel,"setNodeLabel"},
  {(Myinterfun)sci_gateway,sci_setEdgeLabel,"setEdgeLabel"},
  {(Myinterfun)sci_gateway,sci_setEdgeWeight,"setEdgeWeight"},
  {(Myinterfun)sci_gateway,sci_getGraph,"getGraph"},
  {(Myinterfun)sci_gateway,sci_removeGraph,"removeGraph"},
  {(Myinterfun)sci_gateway,sci_clearAllGraphs,"clearAllGraphs"},
  {(Myinterfun)sci_gateway,sci_breadthFirstSearch,"breadthFirstSearch"},
  {(Myinterfun)sci_gateway,sci_depthFirstSearch,"depthFirstSearch"},
  {(Myinterfun)sci_gateway,sci_dijkstraSP,"dijkstraSP"},
  {(Myinterfun)sci_gateway,sci_bellmanFordSP,"bellmanFordSP"},
};
 
int C2F(libbglgraphgateway)()
{
  Rhs = Max(0, Rhs);
  if (*(Tab[Fin-1].f) != NULL) 
  {
     pvApiCtx->pstName = (char*)Tab[Fin-1].name;
    (*(Tab[Fin-1].f))(Tab[Fin-1].name,Tab[Fin-1].F);
  }
  return 0;
}
