// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

#include <string>
#include <iostream>
#include "sciprint.h"

/* ==================================================================== */
#include "iohelper.hpp"
#include "Scierror.h"
/* ==================================================================== */

//
// printGraph(BglGraph graph)
// prints a graph to the Scilab console
//

extern "C" int sci_printGraph (char *fname)
{
    BglGraph * graph;

    /* check that we have 1 parameters input */
    CheckRhs(1,1) ;

    /* check that we have from 0 to 1 parameters output */
    CheckLhs(0,1) ;

    graph = (BglGraph*)(readPointerArgument(1));

    graph->printGraphToStream(cout); //this is redirected to scilab console!
    return 0;
}
/* ==================================================================== */
