// ====================================================================
// Balsa Raicevic
// Boost.Graph Metanet toolbox
// This file is released into the public domain
// ====================================================================

/* ==================================================================== */
#include "iohelper.hpp"
#include "Scierror.h"
/* ==================================================================== */

//
// setEdgeLabel(myGraph, edgeLabel OR edgeIndex , string newLabel)
// This function finds an edge within a graph, and sets its label
//
extern "C" int sci_setEdgeLabel (char *fname)
{
    BglGraph* graph;

    /* check that we have 3 parameters input */
    CheckRhs(3,3) ;

    /* check that we have 0-1 parameters output */
    CheckLhs(0,1) ;


    graph = (BglGraph*)(readPointerArgument(1));
    if (graph == NULL) {
        Scierror(999, "Can't retrieve graph");
        return 0;
    }
    Edge edge = getEdgeFromGraph(graph, 2);
    string newLabel = readStringArgument(3);
    graph->setEdgeLabel(edge, newLabel);
    return 0;
}
/* ==================================================================== */
