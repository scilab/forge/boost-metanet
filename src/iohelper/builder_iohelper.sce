// ====================================================================
// Allan CORNET
// Simon LIPP
// INRIA 2008
// This file is released into the public domain
// ====================================================================

src_dir = get_absolute_file_path('builder_iohelper.sce');
exec(src_dir+'src/builder_src.sce');			
clear src_dir;
