#ifndef __IO_HELPER_HPP_
#define __IO_HELPER_HPP_

#include "sciprint.h"
#include "api_scilab.h"
#include "stack-c.h"
#include "sciprint.h"
#include "Scierror.h"
#include <string>
#include <iostream>

//there must be a better way, but despite the -I path/to/header, builder.sce doesn't see this!
#include "../../bglgraph/includes/bglgraph.hpp"
using namespace std;

#define DEBUG 1

#define BGL_ERROR  \
		if (sciErr.iErr) { \
			printError(&sciErr, 0); \
			return 0; \
		}

#define BGL_ERROR_VOID  \
		if (sciErr.iErr) { \
			printError(&sciErr, 0); \
		}

string readStringArgument(int argNumber);
size_t readIntArgument(int argNumber);
void* readPointerArgument(int argNumber);
double readDoubleArgument(int argNumber);
graph_traits<Graph>::vertex_descriptor getNodeFromGraph(BglGraph* graph, int argNumber);
Edge getEdgeFromGraph(BglGraph* graph, int argNumber);

void writeDoubleArray(vector<double> array, int outputArgNumber);
void writeStringArray(vector<string> array, int outputArgNumber);

template <typename T>
void writeToUnsignedIntArray(vector<unsigned int> vectorToCopy, int outputArgNumber) {
	SciErr sciErr;
	size_t n = vectorToCopy.size();
	unsigned int* array = new unsigned int[n];
	copy( vectorToCopy.begin(), vectorToCopy.end(), array);
	sciErr = createMatrixOfUnsignedInteger32(pvApiCtx, outputArgNumber, 1, vectorToCopy.size(), array);
	delete [] array;
	BGL_ERROR_VOID
}

void writeUnsignedIntArray(vector<Size> array, int outputArgNumber);

///////////////////////////////
// For cout/cerr redirection //
///////////////////////////////

class ScilabStream : public std::basic_streambuf<char>
{
public:
    ScilabStream(std::ostream &stream);
    ~ScilabStream();
protected:
    virtual int_type overflow(int_type v);
    virtual std::streamsize xsputn(const char *p, std::streamsize n);
private:
 std::ostream &m_stream;
 std::streambuf *m_old_buf;
 std::string     m_string;
};
#endif
