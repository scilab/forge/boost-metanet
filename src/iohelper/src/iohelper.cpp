#include "../includes/iohelper.hpp"

size_t readIntArgument(int argNumber) {

	SciErr sciErr;
	int *piAddr32 = NULL;

	int *piData = NULL;
	int iRows32, iCols32;
	int iPrec;

	sciErr = getVarAddressFromPosition(pvApiCtx, argNumber, &piAddr32);
	if (sciErr.iErr) {
		printError(&sciErr, 0);
		return 0;
	}


	sciErr = getMatrixOfIntegerPrecision(pvApiCtx, piAddr32, &iPrec);
	if(sciErr.iErr || iPrec != SCI_INT32)
	{
		printError(&sciErr, 0);
		return 0;
	}

	//retrieve dimensions and data
	sciErr = getMatrixOfInteger32(pvApiCtx, piAddr32, &iRows32, &iCols32, &piData);
	if(sciErr.iErr) {
		printError(&sciErr, 0);
		return 0;
	}

	if (iCols32 != 1 || iRows32 != 1) {
		sciprint("Please enter a single int");
		return 0;
	}
	int result = *piData;

	return (size_t)result;
}

string readStringArgument(int argNumber) {

	int length;

	SciErr sciErr;
	int* piAddr = NULL;
	char *stringArgument = NULL;

	int iRows, iCols;
	int iType;

	sciErr = getVarAddressFromPosition(pvApiCtx, argNumber, &piAddr);
	BGL_ERROR

	sciErr = getVarType(pvApiCtx, piAddr, &iType);
	BGL_ERROR

	if (iType != sci_strings ) {
		throw "This is not a string!";
	}

	sciErr = getMatrixOfString(pvApiCtx, piAddr, &iRows, &iCols, NULL, NULL);
	BGL_ERROR

	if (iCols != 1 || iRows != 1) {
		sciprint("Please enter a single string for the graph name");
		return "";
	}

	sciErr = getMatrixOfString(pvApiCtx, piAddr, &iRows, &iCols, &length, NULL);
	BGL_ERROR

	if (length < 0) {
		Scierror(999, "String size must be positive");
		return "";
	}

	if ((stringArgument = (char*)malloc(length+1)) == NULL) {
		Scierror(999, "No memory!!");
		return 0;
	}

	sciErr = getMatrixOfString(pvApiCtx, piAddr, &iRows, &iCols,
			&length, &stringArgument);
	BGL_ERROR

	string result = string(stringArgument);
	free(stringArgument);

	// Finally we made it!
	return result;
}

void* readPointerArgument(int argNumber) {

	int* piAddr         = NULL;
	void* pvPtr         = NULL;

	int iType;
	SciErr sciErr;

	sciErr = getVarAddressFromPosition(pvApiCtx, argNumber, &piAddr);
	BGL_ERROR

	sciErr = getVarType(pvApiCtx, piAddr, &iType);
	BGL_ERROR

	if (iType != sci_pointer) {
		Scierror(999, "Wrong type for input argument %d - pointer expected.\n", argNumber);
		return 0;
	}

	sciErr = getPointer(pvApiCtx, piAddr, &pvPtr);
	BGL_ERROR

	return pvPtr;
}

double readDoubleArgument(int argNumber) {
	SciErr sciErr;
	int *piAddr = NULL;
	double result;

	double *piData = &result;
	int iRows, iCols;

	sciErr = getVarAddressFromPosition(pvApiCtx, argNumber, &piAddr);
	BGL_ERROR

	sciErr = getMatrixOfDouble(pvApiCtx, piAddr, &iRows, &iCols, NULL);
	BGL_ERROR
	if (iCols != 1 || iRows != 1) {
		Scierror(999, "Please enter a single double argument");
		return 0;
	}
	int type;
	sciErr = getVarType(pvApiCtx, piAddr, &type);
	if (type != sci_matrix) {
		Scierror(999, "Please enter a double value");
		return 0;
	}
	BGL_ERROR;
	sciErr = getMatrixOfDouble(pvApiCtx, piAddr, &iRows, &iCols, &piData);
	BGL_ERROR
	return *piData;
}

Vertex getNodeFromGraph(BglGraph* graph, int argNumber) {
	SciErr sciErr;
	int* piAddr = NULL;

	int iType;

	sciErr = getVarAddressFromPosition(pvApiCtx, argNumber, &piAddr);
	if (sciErr.iErr) {
		printError(&sciErr, 0);
		return INVALID_VERTEX;
	}

	sciErr = getVarType(pvApiCtx, piAddr, &iType);
	if(sciErr.iErr) {
		printError(&sciErr, 0);
		return INVALID_VERTEX;
	}
	switch (iType) {
	case sci_strings:
		return graph->getNode(readStringArgument(argNumber));
	case sci_ints:
		return graph->getNode(readIntArgument(argNumber));
	case sci_pointer:
		return (Vertex)readPointerArgument(argNumber);
	case sci_matrix:
		return graph->getNode((size_t)readDoubleArgument(argNumber));
	default:
		sciprint("warning: couldn't retrieve the node from graph");
		return INVALID_VERTEX;
	}
}

Edge getEdgeFromGraph(BglGraph* graph, int argNumber) {
	SciErr sciErr;
	int* piAddr = NULL;

	int iType;

	sciErr = getVarAddressFromPosition(pvApiCtx, argNumber, &piAddr);
	if (sciErr.iErr) {
		printError(&sciErr, 0);
		return Edge();
	}

	sciErr = getVarType(pvApiCtx, piAddr, &iType);
	if(sciErr.iErr) {
		printError(&sciErr, 0);
		return Edge();
	}
	int v1, v2;
	switch (iType) {
	case sci_strings:
		return graph->getEdge(readStringArgument(argNumber));
	case sci_ints:
	case sci_matrix:
		v1 = getNodeFromGraph(graph, argNumber);
		v2 = getNodeFromGraph(graph, argNumber+1);
		return graph->getEdge(v1,v2);
	default:
		sciprint("warning: couldn't retrieve the node from graph");
		return Edge();
	}
}

void writeDoubleArray(vector<double> vectorToCopy, int outputArgNumber) {
	SciErr sciErr;
	size_t n = vectorToCopy.size();
	double* array = new double[n];
	copy( vectorToCopy.begin(), vectorToCopy.end(), array);
	sciErr = createMatrixOfDouble(pvApiCtx, outputArgNumber, 1, vectorToCopy.size(), array);
	delete [] array;
	BGL_ERROR_VOID
}

void writeStringArray(vector<string> array, int outputArgNumber) {
	SciErr sciErr;
	size_t n = array.size();
    char** buffer = new char*[n];
    for (size_t i = 0; i < n; i++) { 
        buffer[i] = new char[array[i].length() + 1];
        if (buffer[i] == NULL) {
            Scierror(999, "No memory");
            return;
        }
        strcpy(buffer[i], array[i].c_str());
    }
    
	sciErr = createMatrixOfString(pvApiCtx, outputArgNumber, 1, array.size(), buffer);
	BGL_ERROR_VOID
    for (size_t i = 0; i < n; i++)
        delete [] buffer[i];
    delete [] buffer;
}

void writeUnsignedIntArray(vector<unsigned int> vectorToCopy, int outputArgNumber) {
    SciErr sciErr;
    size_t n = vectorToCopy.size();
    unsigned int* array = new unsigned int[n];
    copy( vectorToCopy.begin(), vectorToCopy.end(), array);
    sciErr = createMatrixOfUnsignedInteger32(pvApiCtx, outputArgNumber, 1, vectorToCopy.size(), array);
    delete [] array;
    BGL_ERROR_VOID
}

///////////////////////////////
// For cout/cerr redirection //
///////////////////////////////


ScilabStream::ScilabStream(std::ostream &stream) 
  : m_stream(stream)
{
    m_old_buf = stream.rdbuf();
    stream.rdbuf(this);
}

ScilabStream::~ScilabStream()
{
    // output anything that is left
    if (!m_string.empty())
        sciprint("BGL toolbox: %s\n",m_string.c_str());
    
    m_stream.rdbuf(m_old_buf);
}

std::basic_streambuf<char>::int_type ScilabStream::overflow(std::basic_streambuf<char>::int_type v)
{
   if (v == '\n')
   {
       sciprint("BGL toolbox: %s\n",m_string.c_str());
       m_string.clear();
   }
   else
       m_string.push_back(v);
   
   return v;
}

std::streamsize ScilabStream::xsputn(const char *p, std::streamsize n)
{
    m_string.append(p, p + n);
     
    int pos = 0;
    while (pos != std::string::npos)
    {
        pos = m_string.find('\n');
        if (pos != std::string::npos)
        {
            std::string tmp(m_string.begin(), m_string.begin() + pos);
            sciprint("BGL toolbox: %s\n",tmp.c_str());
            m_string.erase(m_string.begin(), m_string.begin() + pos + 1);
        }
    }
     
    return n;
}

ScilabStream scicout(std::cout);
ScilabStream scicerr(std::cerr);
